package josergc;

public class IntVector {
	private int[] ia;
	private int top;
	private int increment;

	public IntVector() {
		this(16,16);
	}

	public IntVector(int initialCapacity,int increment) {
		ia = new int[initialCapacity];
		this.increment = increment;
	}
	
	public int size() {
		return top;
	}
	
	public void add(int i) {
		int iaLength = ia.length;
		if (top == iaLength) {
			int[] tmp = new int[iaLength + increment];
			System.arraycopy(ia,0,tmp,0,iaLength);
			ia = tmp;
		}
		ia[top++] = i;
	}
}
